#!/usr/bin/python3
# -*- coding: UTF-8 -*-

from setuptools import find_packages
from setuptools import setup

setup(
    name='xivo-dxtora',
    version='0.1',
    description='XIVO dxtora daemon',
    maintainer='Avencall',
    maintainer_email='dev@avencall.com',
    url='http://www.xivo.io/',
    license='GPLv3',
    packages=find_packages(),
    scripts=['bin/xivo-dxtora'],
    data_files=[('/etc/xivo-dxtora', ['etc/xivo-dxtora/config.conf'])],
)
